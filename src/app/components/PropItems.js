import React, {useState} from 'react';
import {ListItem, Icon, Button} from 'react-native-elements';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  FlatList,
  SectionList,
  StyleSheet,
} from 'react-native';
import ListItemComponent from '../components/ListItemComponent';

export default function PropItem(params) {
  if (params.index == 0) {
    return (
      <ListItem
        button
        key={params.index}
        disabled
        disabledStyle={styles.disabledItemStyle}>
        <ListItem.Content>
          <ListItem.Title>{params.name}</ListItem.Title>
        </ListItem.Content>
      </ListItem>
    );
  }

  if (params.index == 1) {
    return (
      <ListItem button key={params.index} Component={ListItemComponent}>
        <ListItem.Content>
          <ListItem.Title>{params.name}</ListItem.Title>
        </ListItem.Content>
      </ListItem>
    );
  }

  if (params.index == 2) {
    return (
      <ListItem
        button
        key={params.index}
        containerStyle={styles.itemContainerStyle}>
        <ListItem.Content>
          <ListItem.Title>{params.name}</ListItem.Title>
        </ListItem.Content>
      </ListItem>
    );
  }

  if (params.index == 3) {
    return (
      <ListItem button key={params.index}>
        <ListItem.Content>
          <ListItem.Title>{params.name}</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron color="black" />
      </ListItem>
    );
  }

  if (params.index == 4) {
    return (
      <ListItem button key={params.index}>
        <ListItem.Content>
          <ListItem.Title>{params.name}</ListItem.Title>
        </ListItem.Content>
        <ListItem.CheckBox color="black" />
      </ListItem>
    );
  }
  if (params.index == 5) {
    return (
      <ListItem button key={params.index}>
        <ListItem.Content>
          <ListItem.Title>{params.name}</ListItem.Title>
        </ListItem.Content>
        <ListItem.ButtonGroup buttons={['button1', 'button2']} />
      </ListItem>
    );
  }

  if (params.index == 6) {
    return (
      <ListItem button key={params.index}>
        <ListItem.Content>
          <ListItem.Title>{params.name}</ListItem.Title>
        </ListItem.Content>
        <ListItem.Input placeholder="Enter" style={{paddingRight:10}}/>
      </ListItem>
    );
  }
}

const styles = StyleSheet.create({
  disabledItemStyle: {
    backgroundColor: 'lightgrey',
  },
  itemContainerStyle: {
    backgroundColor: 'green',
  },
});
