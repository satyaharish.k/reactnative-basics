import React, {useState} from 'react';
import {ListItem, Icon} from 'react-native-elements';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  FlatList,
  SectionList,
  StyleSheet,
} from 'react-native';
export default function ListItemComponent(params) {
  return (
    <View backgroundColor="white" height={50}>
      <Text color="black" style={{paddingLeft: 10, paddingTop: 20}}>
        This is a component
      </Text>
    </View>
  );
}
