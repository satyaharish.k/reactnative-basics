import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

//Containers
import Home from '../containers/HomeScreen';
import Checkbox from '../containers/Checkbox';
import Card from '../containers/Card';
import ListMain from '../containers/ListItemsHome';
import RNFlatList from '../containers/ListScreens/RNFlatList';
import ListWithSections from '../containers/ListScreens/ListWithSections';
import ListAccordion from '../containers/ListScreens/Accordion';
import SwipeableList from '../containers/ListScreens/SwipeableListItem';
import Divider from '../containers/Divider';
import ListItemProps from '../containers/ListScreens/Props';
const Stack = createNativeStackNavigator();
export default function rs(params) {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Checkbox" component={Checkbox} />
        <Stack.Screen name="Card" component={Card} />
        <Stack.Screen name="Divider" component={Divider} />
        <Stack.Screen name="ListItem-Main" component={ListMain} />
        <Stack.Screen name="ReactNative FlatList" component={RNFlatList} />
        <Stack.Screen name="ListWithSections" component={ListWithSections} />
        <Stack.Screen name="ListAccordion" component={ListAccordion} />
        <Stack.Screen name="SwipeableList" component={SwipeableList} />
        <Stack.Screen name="ListItemProps" component={ListItemProps} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
