import React from 'react';
import {Button} from 'react-native-elements';
// import SafeAreaView1 from 'react-native-safe-area-context';
import {SafeAreaView, View} from 'react-native';

import Style from '../styles/Styles';
export default function HomeScreen({navigation}) {
  return (
    <SafeAreaView style={Style.homeStyle}>
      <View style={Style.homeStyle}>
        <Button
          title="Checkbox"
          containerStyle={Style.buttonStyle}
          onPress={() => {
            navigation.navigate('Checkbox');
          }}
        />
        <Button
          title="Card"
          containerStyle={Style.buttonStyle}
          onPress={() => {
            navigation.navigate('Card');
          }}
        />
        <Button
          title="ListItem"
          containerStyle={Style.buttonStyle}
          onPress={() => {
            navigation.navigate('ListItem-Main');
          }}
        />
        <Button
          title="Divider"
          containerStyle={Style.buttonStyle}
          onPress={() => {
            navigation.navigate('Divider');
          }}
        />
      </View>
    </SafeAreaView>
  );
}
