import React from 'react';
import {ListItem} from 'react-native-elements';
import {SafeAreaView, Text, SectionList, StyleSheet} from 'react-native';
import Style from '../../styles/Styles';

export default function ListWithSection(navigator) {
  const colorData = [
    {
      color: 'red',
      value: '#f00',
    },
    {
      color: 'green',
      value: '#0f0',
    },
    {
      color: 'blue',
      value: '#00f',
    },
    {
      color: 'cyan',
      value: '#0ff',
    },
    {
      color: 'magenta',
      value: '#f0f',
    },
    {
      color: 'yellow',
      value: '#ff0',
    },
    {
      color: 'black',
      value: '#000',
    },
  ];
  const fruitData = [
    {
      color: 'Apple',
      value: '#f00',
    },
    {
      color: 'Orange',
      value: '#0f0',
    },
    {
      color: 'Mango',
      value: '#00f',
    },
    {
      color: 'Banana',
      value: '#0ff',
    },
    {
      color: 'StrawBerry',
      value: '#f0f',
    },
    {
      color: 'WaterMelon',
      value: '#ff0',
    },
    {
      color: 'PineApple',
      value: '#000',
    },
  ];
  const renderItem = ({item}) => (
    <ListItem topDivider>
      <ListItem.Content>
        <ListItem.Title>{item.color}</ListItem.Title>
        <ListItem.Subtitle>{item.value}</ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );

  const renderSectionHeader = ({section}) => (
    <Text style={styles.sectionHeader}>{section.title}</Text>
  );

  const key = (item, index) => index.toString();

  return (
    <SafeAreaView>
      <SectionList
        keyExtractor={key}
        sections={[
          {title: 'Colors', data: colorData},
          {title: 'Fruits', data: fruitData},
        ]}
        renderItem={renderItem}
        renderSectionHeader={renderSectionHeader}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  sectionHeader: {
    height: 40,
    paddingTop: 10,
    paddingLeft: 10,
    fontWeight: 'bold',
  },
});
