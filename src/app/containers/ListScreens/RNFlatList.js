import React from 'react';
import {ListItem, Icon} from 'react-native-elements';
import {SafeAreaView, ScrollView, Text, FlatList} from 'react-native';
import Style from '../../styles/Styles';

export default function RNFlatList(navigator) {
  const data = [
    {
      color: 'red',
      value: '#f00',
    },
    {
      color: 'green',
      value: '#0f0',
    },
    {
      color: 'blue',
      value: '#00f',
    },
    {
      color: 'cyan',
      value: '#0ff',
    },
    {
      color: 'magenta',
      value: '#f0f',
    },
    {
      color: 'yellow',
      value: '#ff0',
    },
    {
      color: 'black',
      value: '#000',
    },
  ];
  const renderItem = ({item}) => (
    <ListItem bottomDivider>
      <Icon name="location" />
      <ListItem.Content>
        <ListItem.Title>{item.color}</ListItem.Title>
        <ListItem.Subtitle>{item.value}</ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );

  const key = (item, index) => index.toString();

  return (
    <SafeAreaView>
      <FlatList
        keyExtractor={key}
        data={data}
        renderItem={renderItem}
        ListFooterComponent={<Text>This is Footer</Text>}
        ListHeaderComponent={<Text>This is Header</Text>}
        ListHeaderComponentStyle={{
          height: 50,
          paddingLeft: 10,
          paddingTop: 20,
        }}
      />
    </SafeAreaView>
  );
}
