import React, {useState} from 'react';
import {ListItem, Icon} from 'react-native-elements';
import {
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Style from '../../styles/Styles';
import ListItemComponent from '../../components/ListItemComponent';
import PropItem from '../../components/PropItems';

export default function ListMain({navigation}) {
  const [checked, setChecked] = useState(false);
  const items = [
    'Disabled',
    'Component',
    'containerStyle',
    'chevron',
    'Checkbox',
    'ButtonGroup',
    'Input',
  ];

  return (
    <SafeAreaView>
      {items.map((name, i) => (
        <PropItem index={i} name={name} />
      ))}
    </SafeAreaView>
  );
}
