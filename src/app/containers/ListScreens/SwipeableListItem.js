import React from 'react';
import {ListItem, Button} from 'react-native-elements';
import {SafeAreaView, ScrollView, Text, FlatList} from 'react-native';
import Style from '../../styles/Styles';

export default function SwipeableList(navigator) {
  const data = [
    {
      color: 'red',
      value: '#f00',
    },
    {
      color: 'green',
      value: '#0f0',
    },
    {
      color: 'blue',
      value: '#00f',
    },
    {
      color: 'cyan',
      value: '#0ff',
    },
    {
      color: 'magenta',
      value: '#f0f',
    },
    {
      color: 'yellow',
      value: '#ff0',
    },
    {
      color: 'black',
      value: '#000',
    },
  ];
  const renderItem = ({item}) => (
    <ListItem.Swipeable
      bottomDivider
      leftContent={
        <Button
          title="Info"
          icon={{name: 'info', color: 'white'}}
          buttonStyle={{minHeight: '100%'}}
        />
      }
      rightContent={
        <Button
          title="Delete"
          icon={{name: 'delete', color: 'white'}}
          buttonStyle={{minHeight: '100%', backgroundColor: 'red'}}
        />
      }>
      <ListItem.Content>
        <ListItem.Title>{item.color}</ListItem.Title>
        <ListItem.Subtitle>{item.value}</ListItem.Subtitle>
      </ListItem.Content>
    </ListItem.Swipeable>
  );

  const key = (item, index) => index.toString();

  return (
    <SafeAreaView>
      <FlatList keyExtractor={key} data={data} renderItem={renderItem} />
    </SafeAreaView>
  );
}
