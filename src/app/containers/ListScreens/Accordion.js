import React, {useState} from 'react';
import {ListItem, Icon} from 'react-native-elements';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  FlatList,
  SectionList,
  StyleSheet,
} from 'react-native';
import Style from '../../styles/Styles';

export default function AccordionList(navigation) {
  const [checked, setChecked] = useState(false);
  const colorData = [
    {
      color: 'red',
      value: '#f00',
    },
    {
      color: 'green',
      value: '#0f0',
    },
    {
      color: 'blue',
      value: '#00f',
    },
    {
      color: 'cyan',
      value: '#0ff',
    },
    {
      color: 'magenta',
      value: '#f0f',
    },
    {
      color: 'yellow',
      value: '#ff0',
    },
    {
      color: 'black',
      value: '#000',
    },
  ];
  const fruitData = [
    {
      color: 'Apple',
      value: '#f00',
    },
    {
      color: 'Orange',
      value: '#0f0',
    },
    {
      color: 'Mango',
      value: '#00f',
    },
    {
      color: 'Banana',
      value: '#0ff',
    },
    {
      color: 'StrawBerry',
      value: '#f0f',
    },
    {
      color: 'WaterMelon',
      value: '#ff0',
    },
    {
      color: 'PineApple',
      value: '#000',
    },
  ];
  const renderItem = ({item}) => (
    <ListItem bottomDivider>
      <ListItem.Content>
        <ListItem.Title>{item.color}</ListItem.Title>
        <ListItem.Subtitle>{item.value}</ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );

  const key = (item, index) => index.toString();

  return (
    <SafeAreaView>
      <ListItem.Accordion
        // icon={{name: 'info', color: 'black'}}
        // expandIcon={{name: 'close', color: 'black'}}
        // noIcon
        // noRotation
        content={
          <>
            <Icon name="place" size={30} />
            <ListItem.Content>
              <ListItem.Title>Fruits</ListItem.Title>
            </ListItem.Content>
          </>
        }
        isExpanded={checked}
        onPress={() => {
          setChecked(!checked);
        }}>
        {fruitData.map((item, i) => (
          <ListItem button key={i}>
            <ListItem.Content>
              <ListItem.Title>{item.color}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        ))}
      </ListItem.Accordion>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  sectionHeader: {
    height: 40,
    paddingTop: 10,
    paddingLeft: 10,
    fontWeight: 'bold',
  },
});
