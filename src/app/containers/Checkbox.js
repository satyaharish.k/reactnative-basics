import React, {useState} from 'react';
import {Button, CheckBox} from 'react-native-elements';
import {SafeAreaView, View, ScrollView, Text, Image} from 'react-native';
import Style from '../styles/Styles';
export default function Checkbox(params) {
  const [checked, setChecked] = useState(false);
  const [checkedTitle, setCheckedTitle] = useState(false);
  return (
    <SafeAreaView style={Style.homeStyle}>
      <ScrollView>
        <CheckBox containerStyle={Style.CheckBoxStyle} />
        <CheckBox center />
        <CheckBox right />
        <CheckBox title="Accept Privacy policy" />
        <CheckBox
          title="Accept Privacy policy"
          textStyle={Style.CheckBoxTextStyle}
        />
        <CheckBox checked />
        <CheckBox checked checkedColor="red" />
        <CheckBox
          checked={checked}
          title="Accept Privacy policy"
          onPress={() => setChecked(!checked)}
        />
        <CheckBox
          checked={checkedTitle}
          title="Accept Privacy policy"
          onPress={() => setCheckedTitle(!checkedTitle)}
          checkedTitle="Reject Privacy policy"
        />
        <CheckBox
          checked={checkedTitle}
          title="Accept Privacy policy"
          onPress={() => setCheckedTitle(!checkedTitle)}
          checkedTitle="Reject Privacy policy"
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
        />
        <CheckBox
          checked={checkedTitle}
          title="Accept Privacy policy"
          onPress={() => setCheckedTitle(!checkedTitle)}
          iconType="material"
          checkedTitle="Reject Privacy policy"
          checkedIcon="clear"
          uncheckedIcon="add"
        />
        <CheckBox
          checked={checkedTitle}
          title="Accept Privacy policy"
          onPress={() => setCheckedTitle(!checkedTitle)}
          checkedTitle="Reject Privacy policy"
          checkedIcon={
            <Image source={require('../assets/images/favChecked.png')} />
          }
          uncheckedIcon={
            <Image source={require('../assets/images/favUnChecked.png')} />
          }
        />
        <CheckBox
          style={Style.checkBoxRight}
          checked={checkedTitle}
          title="Accept Privacy policy"
          onPress={() => setCheckedTitle(!checkedTitle)}
          checkedTitle="Reject Privacy policy"
          checkedIcon={
            <Image source={require('../assets/images/favChecked.png')} />
          }
          uncheckedIcon={
            <Image source={require('../assets/images/favUnChecked.png')} />
          }
          iconRight={true}
          textStyle={{marginRight: 75}}
          right
        />
      </ScrollView>
    </SafeAreaView>
  );
}
