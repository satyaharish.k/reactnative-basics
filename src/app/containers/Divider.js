import React, {useState} from 'react';
import {Button, CheckBox, Divider} from 'react-native-elements';
import {
  SafeAreaView,
  View,
  ScrollView,
  Text,
  Image,
  StyleSheet,
} from 'react-native';
import Style from '../styles/Styles';
export default function Checkbox(params) {
  const [checked, setChecked] = useState(false);
  const [checkedTitle, setCheckedTitle] = useState(false);
  return (
    <SafeAreaView
      style={{
        backgroundColor: 'white',
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
      }}>
      <View flex={6} backgroundColor="green" />
      <Divider
        orientation="vertical"
        color="red"
        width={5}
        inset
        insetType="right"
      />
      <View flex={3} backgroundColor="yellow" />
      <Divider
        orientation="vertical"
        color="red"
        width={5}
        insetType="center"
      />
      <View flex={3} backgroundColor="blue" />
    </SafeAreaView>
  );
}
