import React from 'react';
import {Button, Card, Icon} from 'react-native-elements';
import {SafeAreaView, ScrollView, Text} from 'react-native';
import Style from '../styles/Styles';

export default function CardScreen(params) {
  return (
    <SafeAreaView style={Style.cardScreenStyle}>
      <ScrollView>
        <Card
          containerStyle={Style.cardStyle}
          wrapperStyle={Style.cardInnerStyle}>
          <Card.Title>CARD WITH TITLE</Card.Title>
        </Card>
        <Card containerStyle={Style.cardStyle}>
          <Card.Title>CARD WITH DIVIDER</Card.Title>
          <Card.Divider
            color="red"
            inset="true"
            insetType="right"
            subHeader="Hello There"
            orientation="horizontal"
            width={5}
          />
        </Card>
        <Card containerStyle={Style.cardStyle}>
          <Card.Title>CARD WITH FEATURED TITLE</Card.Title>
          <Card.FeaturedTitle style={Style.cardFeaturedStyle}>
            Hello
          </Card.FeaturedTitle>
          <Card.FeaturedSubtitle style={Style.cardFeaturedStyle}>
            Hello There
          </Card.FeaturedSubtitle>
        </Card>
        <Card containerStyle={Style.cardStyle}>
          <Card.Title>CARD WITH IMAGE</Card.Title>
          <Card.Divider
            color="red"
            insetType="middle"
            orientation="horizontal"
          />
          <Card.Image
            source={require('../assets/images/favChecked.png')}
            resizeMode="contain"
          />
          <Text style={Style.cardDescriptionStyle}>
            This is Image Description
          </Text>
          <Button title="Next" />
        </Card>
      </ScrollView>
    </SafeAreaView>
  );
}
