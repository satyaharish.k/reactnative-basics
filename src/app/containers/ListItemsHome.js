import React, {useState} from 'react';
import {ListItem, Icon} from 'react-native-elements';
import {
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Style from '../styles/Styles';

export default function ListMain({navigation}) {
  const [checked, setChecked] = useState(false);
  const items = [
    //   {a:""}
    'ReactNative FlatList',
    'ListWithSections',
    'ListAccordion',
    'SwipeableList',
    'ListItemProps',
  ];

  return (
    <SafeAreaView>
      <ScrollView>
        {items.map((name, i) => (
          <ListItem
            button
            key={i}
            onPress={() => {
              navigation.navigate({name});
            }}>
            <ListItem.Content>
              <ListItem.Title>{name}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  disabledItemStyle: {
    backgroundColor: 'lightgrey',
  },
});
