import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  homeStyle: {
    backgroundColor: 'white',
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'center',
  },
  cardDividerStyle: {
    marginTop: 0,
  },

  buttonStyle: {
    color: 'green',
    padding: 10,
    marginLeft: '10%',
    marginRight: '10%',
  },

  CheckBoxStyle: {
    padding: 20,
  },

  CheckBoxTextStyle: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },

  cardScreenStyle: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },

  cardStyle: {
    flex: 1,
    flexDirection: 'column',
    padding: 10,
    shadowColor: 'red',
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
  },
  cardInnerStyle: {
    justifyContent: 'center',
  },
  cardTitleStyle: {
    alignSelf: 'flex-start',
  },
  cardDescriptionStyle: {
    marginBottom: 10,
  },
  cardButtonStyle: {
    marginLeft: '10%',
    marginRight: '10%',
    marginBottom: 10,
  },
  cardFeaturedStyle: {
    color: 'black',
  },
  checkBoxRight: {
    alignSelf: 'flex-end',
  },
});
